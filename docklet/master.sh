
set -e

cell='th-master'
keyspace='template'
shard=0
uid_base=100
tablet_type='master'
port_base=15200
grpc_port_base=16200
mysql_port_base=33100

script_root=`dirname "{BASH_SOURCE}"`
source $script_root/env.sh

dbconfig_flags="\
	-db-config-app"